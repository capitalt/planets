# Star Wars Planets

A frontend for the [Star Wars API planet resource](https://swapi.co/documentation#planets). Browse Star Wars planets in a table with column sorting and pagination, and filter results by keyword.

Uses [Vue.js](https://vuejs.org/), [Vuetify](https://vuetifyjs.com/en/) and [RxJS](https://rxjs-dev.firebaseapp.com/).

## Demo

[https://thompsonsj.com/planets/](https://thompsonsj.com/planets/).

## Development

Star Wars Planets is a [Vue CLI](https://cli.vuejs.org/) project.

To get started, run the install command for your preferred package manager in the root directory.

```
yarn install
# Alternatively, use npm:
npm install
```

The `build`, `serve` and `lint` scripts are available as part of the Vue CLI project.

```
# Build the project into a `dist` folder.
yarn build
# Serve the project locally with hot reloading.
yarn serve
# Validate against ESLint standards.
yarn lint
```

## Engineering decisions

### RxJS

This project integrates [RxJS](https://rxjs-dev.firebaseapp.com/) in order to handle complex user input and API call logic effectively. Observables and data streams are used to:

- only make API calls when a value has changed,
- restrict the amount of time between subsequent API calls, and
- cancel any previous API calls if a new call is made.

### Films

Star Wars films are listed for each planet, and an API call is required to retrieve the desired film details (in this case: film title). In order to significantly decrease the number of API calls made, all Star Wars films are cached on load and the request that is performed is synchronous to ensure the data is available to subsequent API operations.

This consideration was made because there are not many film records (7 in total).

Future versions of this application may wish to:

- Implement API calls per result.
- Improve the way films are loaded to be less detrimental to the user experience (synchronous calls increase initial load time).

### Vuetify

The [Vuetify](https://vuetifyjs.com/en/) framework has been adopted in order to achieve the following.

- Ensure browser compatibility (Note: IE9/IE10 are not supported).
- Achieve a responsive layout to ensure the application works well on a range of screen sizes.
- Implement interactive components with Vue such as the text field, pagination and the data table with column sorting.

To reduce bloat, only the required components are loaded from this framework.

## Testing

[TestCafe](http://devexpress.github.io/testcafe/) is added as a development dependency and is configured to perform automated end-to-end web testing of the application.

By default, tests are configured to be run on `http://localhost:8080`. As a result, you may wish to run `yarn serve`/`npm run serve` before running tests.

For convenience, the following commands are included in `package.json`.

```
yarn test:chrome
yarn test:chromium
yarn test:firefox
```
