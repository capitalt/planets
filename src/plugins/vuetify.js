import Vue from 'vue'
import {
  Vuetify,
  VApp,
  VDataTable,
  VForm,
  VIcon,
  VGrid,
  VPagination,
  VTextField,
  transitions
} from 'vuetify'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  components: {
    VApp,
    VDataTable,
    VForm,
    VIcon,
    VGrid,
    VPagination,
    VTextField,
    transitions
  },
})
