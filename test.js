import {
  ClientFunction,
	Selector
} from 'testcafe';

fixture `Test the Star Wars Planets frontend.`
	.page `http://localhost:8081/`;

test(`Search for 'Rodia'.`, async t => {
		await t
      .typeText('input', 'Rodia');

    const rodiaRow = Selector('tbody > tr').withText('Rodia');

    await t
      .expect(rodiaRow.exists).ok();
});

test(`Search for 'Rhodia' (no results).`, async t => {
		await t
      .typeText('input', 'Rhodia');

    const rhodiaRow = Selector('tbody > tr').withText('Rhodia');

    await t
      .expect(rhodiaRow.exists).notOk();
});
